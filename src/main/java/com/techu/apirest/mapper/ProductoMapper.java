package com.techu.apirest.mapper;

import com.techu.apirest.model.ProductoModel;

public class ProductoMapper {

    private ProductoModel mapPersistenceProductoModel(ProductoModel producto){
        ProductoModel productoModel = new ProductoModel();
        productoModel.setId(producto.getId());
        productoModel.setDescripcion(producto.getDescripcion());
        productoModel.setPrecio(producto.getPrecio());
        return productoModel;
    }

    private void mapRestModelToPersistenceproductoModel(ProductoModel productoModel, ProductoModel producto ){
        producto.setDescripcion(productoModel.getDescripcion());
        producto.setPrecio(productoModel.getPrecio());
    }
}
