package com.techu.apirest.controller;

import com.techu.apirest.model.ProductoModel;
import com.techu.apirest.service.ProductoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v2")
public class ProductoController {

    @Autowired
    ProductoService productoService;

    @GetMapping("/productos")
    public List<ProductoModel> getProductos() {
        return productoService.findAll();
    }

    @GetMapping("/productos/{id}" )
    public Optional<ProductoModel> getProductoId(@PathVariable String id){
        return productoService.findById(id);
    }

    @PostMapping("/productos")
    public ProductoModel postProductos(@RequestBody ProductoModel newProducto){
        productoService.save(newProducto);
        return newProducto;
    }

    @PutMapping("/productos")
    public void putProductos(@RequestBody ProductoModel productoToUpdate){
        productoService.save(productoToUpdate);
    }

    @PutMapping("/productos/{id}")
    public boolean putProductos(@RequestBody ProductoModel productoToUpdate, @PathVariable String id) {
        if (productoService.findById(id).isPresent()){
            productoService.save(productoToUpdate);
            return true;
        }
            return false;
    }

    @DeleteMapping("/productos")
    public boolean deleteProductos(@RequestBody ProductoModel productoToDelete){
        return productoService.delete(productoToDelete);
    }

    @DeleteMapping("/productos/{id}")
    public boolean deleteProductos(@PathVariable String id){
        if (productoService.findById(id).isPresent()){
            productoService.deleteById(id);
            return true;
        }
            return false;
    }

    @PatchMapping("/productos/{id}")
    public ProductoModel patchProductos(@RequestBody ProductoModel productoToPatch, @PathVariable String id) {
        if (productoService.findById(id).isPresent()) {
            Optional <ProductoModel> oldProducto = productoService.findById(id);

            if(productoToPatch.getDescripcion() != null){
                oldProducto.get().setDescripcion(productoToPatch.getDescripcion());
            }

            if(productoToPatch.getPrecio() != null){
                oldProducto.get().setPrecio(productoToPatch.getPrecio());
            }

          productoService.save(oldProducto.get());
          return oldProducto.get();
        }
        return null;
    }
}
